# App Deployment Configuration for ArgoCD

`base/` contains the core configuration: deployment, ingress, and service for a simple app
`overlays/` contains the env specific configuration to overide base with
