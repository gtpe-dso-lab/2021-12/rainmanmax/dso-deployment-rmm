# App Deployment Configuration for ArgoCD

`base/` contains the core configuration: deployment, ingress, and service for a simple app
`overlays/` contains the env specific configuration to overide base with

Check all files, and replace `NN` with your assigned student number - e.g., 01, 16, etc.
